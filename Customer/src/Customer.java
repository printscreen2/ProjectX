public class Customer {
	private String UseriD;
	private String name;
	private int numItems;

	public int getNumItems() {
		return numItems;
	}
	public void setNumItems(int numItems) {
		this.numItems = numItems;
	}
	Customer() 
	{
		name = "";
		UseriD = "00000";
		numItems = 0;
	}
	Customer(String n,String id, int i  )
	{
		name = n;
		UseriD = id;
		numItems = i; 
	}
	public final String getName() 
	{
		return name;
	}
	public void setName(String Cusname){
		this.name = Cusname; 
	}
	public final String getUserID() 
	{
		return UseriD;
	}
	public void setUserID(String ID)
	{
		this.UseriD = ID;
	}

}
