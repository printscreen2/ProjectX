
public class Main {
	public static void main(String [] args){
		Customer cus1= new Customer();
		Customer cus2 = new Customer();
		
		cus1.setName("Jorge");
		cus2.setName("Bob");
		cus1.setUserID("1a2b3c4");
		cus2.setUserID("5d6e7f8");
		cus1.setNumItems(5);
		cus2.setNumItems(60);
		
		System.out.println("Customer 1 ");
		System.out.println("Name: " + cus1.getName());
		System.out.println("User ID: " + cus1.getUserID());
		System.out.println("Number of Items: " + cus1.getNumItems());
		System.out.println("=============================================================");
		System.out.println("Customer 2 ");
		System.out.println("Name: " + cus2.getName());
		System.out.println("User ID: " + cus2.getUserID());
		System.out.println("Number of Items: " + cus2.getNumItems());
	}

}
