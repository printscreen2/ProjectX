import java.util.*;

public class Math {
	static int answer1 = 0;
	static int answer2 = 0;
	static int answer3 = 0;
	static double answer4 = 0;
	static String last;/* This Stores the return value for the math portion */
	static String biggest;/*
							 * This Stores the return value for the comparison
							 * portion
							 */

	public String doit(int num1, int num2) {
		/***
		 * Assignment 1 This is the function in the program that adds,
		 * subtracts, multiplies 2 numbers (num1 and num2) and sets them to
		 * their perspective answers.
		 */
		if (num1 == 0) {
			return "0";
		} else if (num2 == 0)
			return "0";
		else {
			answer1 = num1 + num2;
			answer2 = num1 - num2;
			answer3 = num1 * num2;
			answer4 = num1 / num2;
			last = "Sum= " + Integer.toString(answer1) + " " + "Subtraction = " + Integer.toString(answer2) + " "
					+ "Multiplication = " + Integer.toString(answer3) + " " + "Divide = " + Double.toString(answer4);

		}
		return last;

	}

	public String greatest(int a, int b, int c) {
		/***
		 * Assignment 2 
		 * This is the function in the class that compares 3
		 * numbers that the User inputs
		 */
		if (a > b & a > c)
			biggest = Integer.toString(a) + " is the biggest number";
		else if (b > a & b > c)
			biggest = Integer.toString(b) + " is the biggest number";
		else if (c > a & c > b)
			biggest = Integer.toString(c) + " is the biggest number";
		else if (a == b & a > c || a == c & a < b)
			biggest = Integer.toString(a) + " is the biggest number";
		else if (b == a & b > c || b == c & b < a)
			biggest = Integer.toString(b) + " is the biggest number";
		else if (c == a & c > b || c == b & c > a)
			biggest = Integer.toString(c) + " is the biggest number";
		else	
			biggest = "All the numbers are equal";

		return biggest;
	}

	public static void main(String[] args) {

		Math tree = new Math();
		boolean a = true, b = false;
		int c, d, e, f, g, h, i;
		Scanner in = new Scanner(System.in);

		System.out.println("Enter the First number: ");
		c = in.nextInt();
		System.out.println("Enter the Second number: ");
		d = in.nextInt();
		System.out.println("Enter the Third number: ");
		e = in.nextInt();
		System.out.println(tree.greatest(c, d, e));// Assignment 2
		System.out.println(tree.doit(1002, 4007));// Assignment 1
		/***
		 * Assignment 3
		 */
		System.out.println("====================================");
		System.out.println(!a);
		System.out.println(a | b);
		System.out.println((!a & b) | (a & !b));

	}

}
